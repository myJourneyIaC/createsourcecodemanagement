variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com/api/v4/"
}

variable "gitlab_token" {
  type    = string
}

variable "_visibility_level_" {
  type    = string
  default = "private"
}

### Create new project ####

# Name Main Project
variable "_name_new_project_" {
  type = string
  description = "Name Main Project Group"
}

# Description Main Project
variable "_description_new_project_" {
  type = string
  description = "Description Main Project Group"
}

# Main Group of this project will be belong
variable "_main_group_id_" {
  type = string
  description = "Main Group id"
}

variable "_managed_by_apps_" {
  type = string
  description = "Main Group id"
  default = "Terraform"
}

variable "_project_creator_" {
  type = string
  description = "Name Main Project Creator"
  default = "edward.josette@gmail.com"
}

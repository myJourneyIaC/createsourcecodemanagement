
output "new_project_id" {
  value = "${gitlab_project.new_project.id}"
}

output "new_project_url" {
  value = "${gitlab_project.new_project.web_url}"
}

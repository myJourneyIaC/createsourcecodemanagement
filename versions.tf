terraform {
  required_providers {
    gitlab = {
      source = "terraform-providers/gitlab"
    }
  }
  required_version = ">= 0.13"
}

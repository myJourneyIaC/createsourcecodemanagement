# Configure the GitLab Provider
# With my personal token ttf = 1day since 28/10 - 31/10

provider "gitlab" {
  token    = var.gitlab_token
  base_url = var.gitlab_url
}

# Create new project
resource "gitlab_project" "new_project" {
  name             = var._name_new_project_
  description      = var._description_new_project_
  namespace_id     = var._main_group_id_
  visibility_level = var._visibility_level_
  default_branch   = "master"
  tags  = [ "managed_by = Terraform" ,
            "created_by = ${var._project_creator_}"
          ]
  
}
resource "gitlab_branch_protection" "BranchProtect" {
  project = gitlab_project.new_project.id
  branch = "master"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}